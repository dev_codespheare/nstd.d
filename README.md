# nstd.d #

A collection of D programming language modules and utilities.



### What is this repository for? ###

boolean - enumeration constants representing boolean values
enumeration - enumeration queries and string conversions
lettercase - case sensitivity enumeration constants and type



### How do I get set up? ###

These modules and utilities do not have external dependencies other than
the D programming language standard library, _phobos_.

* Clone the repository
* Create symbolic link to `src` directory

