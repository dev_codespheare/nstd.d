module exp.boolean;



/**
 * Yes and no alternative identifiers for boolean values.
 */
enum YesNo: bool {
    No = false,
    Yes = true
}
unittest
{
    import std.traits: EnumMembers;
    static assert((EnumMembers!YesNo).length == 2);

    static assert (__traits(identifier, EnumMembers!YesNo[0]) == "No");
    static assert (__traits(identifier, EnumMembers!YesNo[1]) == "Yes");

    enum members = __traits(allMembers, YesNo);
    static assert (members[0] == "No");
    static assert (members[1] == "Yes");
}



/**
 * On and off alternative identifiers for boolean values.
 */
enum OnOff: bool {
    Off = false,
    On = true
}
unittest
{
    import std.traits: EnumMembers;
    static assert((EnumMembers!OnOff).length == 2);

    static assert (__traits(identifier, EnumMembers!OnOff[0]) == "Off");
    static assert (__traits(identifier, EnumMembers!OnOff[1]) == "On");

    enum members = __traits(allMembers, OnOff);
    static assert (members[0] == "Off");
    static assert (members[1] == "On");
}
