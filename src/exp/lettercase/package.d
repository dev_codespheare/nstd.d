module exp.lettercase;



/**
 * Letter case sensitivity enumeration.
 */
enum Sensitive: bool {
    No,
    Yes
}
unittest
{
    import std.traits: EnumMembers;
    static assert((EnumMembers!Sensitive).length == 2);

    static assert (__traits(identifier, EnumMembers!Sensitive[0]) == "No");
    static assert (__traits(identifier, EnumMembers!Sensitive[1]) == "Yes");

    enum members = __traits(allMembers, Sensitive);
    static assert (members[0] == "No");
    static assert (members[1] == "Yes");
}



/**
 * Letter variant enumeration.
 */
enum Variant: bool {
    Lower,
    Upper
}
unittest
{
    import std.traits: EnumMembers;
    static assert((EnumMembers!Variant).length == 2);

    static assert (__traits(identifier, EnumMembers!Variant[0]) == "Lower");
    static assert (__traits(identifier, EnumMembers!Variant[1]) == "Upper");

    enum members = __traits(allMembers, Variant);
    static assert (members[0] == "Lower");
    static assert (members[1] == "Upper");
}
