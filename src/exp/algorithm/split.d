
struct SplitResult (T)
{
    T[] satisfied;
    T[] unsatisfied;
}



SplitResult!T
split (
    alias pred,
    T
) (const T[] arg)
{
    import std.functional : unaryFun;
    alias predFunc = unaryFun!pred;

    SplitResult!T result;
    foreach (e; arg) {
        if (predFunc(e)) {
            result.satisfied ~= e;
        } else {
            result.unsatisfied ~= e;
        }
    }
    return result;
}
@("split: string")
unittest {
    mixin (unittesting.sut.prologue);
    import std.uni: isLower;
    auto r = split!isLower("aBcDeF");
    assert (r.satisfied == "ace");
    assert (r.unsatisfied == "BDF");
}
@("split: numeric")
unittest {
    mixin (unittesting.sut.prologue);
    import std.uni: isLower;
    auto r = split!(a => a < 5)([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    assert (r.satisfied == [1, 2, 3, 4]);
    assert (r.unsatisfied == [5, 6, 7, 8, 9]);
}
