module exp.enumeration.query;

import exp.enumeration.conversion: value;
import exp.lettercase;

import std.traits: Unqual, EnumMembers, OriginalType, isSomeString;
debug import std.stdio;



/**
 * Detect whether `T` is an `enum` whose base type is some `string`.
 *
 * Returns: `true` if `T` is an `enum` whose base type is some `string`, `false`
 *          otherwise.
 */
enum isEnumString(T) = is(T == enum) && isSomeString!(OriginalType!(Unqual!T));

unittest {
    enum I { Planet, Star, Moon }
    enum S : string { A = "a", B = "b", C = "c" }
    static assert (isEnumString!(I) == false);
    static assert (isEnumString!(S));
}



/**
 * Get the number of members of an enumerated type `T`.
 *
 * ---
 * enum T : int { A, B, C }
 * auto n = count!T;
 * assert (n == 3);
 * ---
 *
 * Returns: The number of members as `size_t`.
 */
size_t
count (T)()
if (is(T == enum))
{
    return (EnumMembers!T).length;
}
unittest
{
    enum I { A }
    enum S : string { A = "one", B = "two", C = "three" }
    assert (count!I == 1);
    assert (count!S == 3);
}



/**
 * Determine whether the string argument is equal to an enumeration member
 * identifier.
 *
 * Returns: `true` if the string argument is equal to an enumeration member
 * identifier.
 */
bool
isId (T)(
    const string arg,
    Sensitive caseSensitive = Sensitive.Yes
)
if (is(T == enum))
{
    version (none) {
        import std.uni: toLower;
        import std.conv: to;

        alias E = Unqual!T;

        if (caseSensitive == Sensitive.Yes) {
            foreach (e; EnumMembers!E) {
                if (arg == to!string(e)) {
                    return true;
                }
            }
        } else {
            const lowercase = toLower(arg);
            foreach (e; EnumMembers!E) {
                if (lowercase == toLower(to!string(e))) {
                    return true;
                }
            }
        }
        return false;
    }

    import std.uni: toLower;
    import std.algorithm.searching: canFind;

    alias UT = Unqual!T;
    alias CStr = const string;

    const members = [__traits(allMembers, UT)];
    if (caseSensitive == Sensitive.Yes) {
        auto exactCase = (CStr e, CStr needle) => e == needle;
        return members.canFind!(exactCase)(arg);
    } else {
        auto lowerCase = (CStr e, CStr needle) => toLower(e) == needle;
        return members.canFind!(lowerCase)(toLower(arg));
    }
}
unittest
{
    enum E { Planet, Star, Moon }

    assert (isId!E("Planet"));
    assert (isId!E("Star"));
    assert (isId!E("Moon"));

    assert (!isId!E("planet"));
    assert (!isId!E("star"));
    assert (!isId!E("moon"));

    assert (!isId!E("PLANET"));
    assert (!isId!E("STAR"));
    assert (!isId!E("MOON"));

    assert (isId!E("planet", Sensitive.No));
    assert (isId!E("star", Sensitive.No));
    assert (isId!E("moon", Sensitive.No));

    assert (isId!E("PLANET", Sensitive.No));
    assert (isId!E("STAR", Sensitive.No));
    assert (isId!E("MOON", Sensitive.No));

    assert (isId!E("Planet", Sensitive.No));
    assert (isId!E("Star", Sensitive.No));
    assert (isId!E("Moon", Sensitive.No));

    assert(!isId!E("asteroid", Sensitive.No));
}



/**
 * Determine whether the string argument is equivalent to an enumeration member
 * value.
 *
 * Returns: `true` if the string argument is equivalent to an enumeration member
 * value.
 */
bool
isValue (T)(
    const string arg,
    Sensitive caseSensitive = Sensitive.Yes
)
if (is(T == enum))
{
    import std.uni: toLower;

    alias UT = Unqual!T;

    if (caseSensitive == Sensitive.Yes) {
        foreach (e; EnumMembers!UT) {
            if (arg == value(e)) {
                return true;
            }
        }
    } else {
        const locaseArg = toLower(arg);
        foreach (e; EnumMembers!UT) {
            if (locaseArg == toLower(value(e))) {
                return true;
            }
        }
    }
    return false;
}
unittest
{
    enum B : bool { F = false, T = true }

    assert(isValue!B("false"));
    assert(isValue!B("true"));
    assert(!isValue!B("TRUE"));

    assert(isValue!B("FALSE", Sensitive.No));
    assert(isValue!B("TRUE", Sensitive.No));
    assert(!isValue!B("x", Sensitive.No));
}
unittest
{
    enum I { F = 100, T = 200 }

    assert(isValue!I("100"));
    assert(isValue!I("200"));
    assert(!isValue!I("300"));

    assert(isValue!I("100", Sensitive.No));
    assert(isValue!I("200", Sensitive.No));
    assert(!isValue!I("300", Sensitive.No));
}
unittest
{
    enum S : string { F = "mercury", T = "mars" }

    assert(isValue!S("mercury"));
    assert(isValue!S("mars"));
    assert(!isValue!S("earth"));

    assert(isValue!S("MERCURY", Sensitive.No));
    assert(isValue!S("MARS", Sensitive.No));
    assert(!isValue!S("EARTH", Sensitive.No));
}
