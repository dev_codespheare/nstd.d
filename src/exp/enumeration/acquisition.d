module exp.enumeration.acquisition;

import exp.enumeration.query;
import exp.enumeration.conversion: value;
import exp.lettercase;

debug import std.stdio;



/**
 * Get the enumeration member identifier matching the string argument.
 * Assumes that the string argument is a valid enumeration member identifier.
 * To check, use the `isId` function.
 *
 * Returns: An enumeration member identifier.
 */
T
getId (T)(
    const string name,
    Sensitive caseSensitive = Sensitive.Yes
)
if (is(T == enum))
{
    import std.traits: Unqual, EnumMembers;
    import std.uni: toLower;
    import std.conv: to;

    alias UT = Unqual!T;

    if (caseSensitive == Sensitive.Yes) {
        foreach (e; EnumMembers!UT) {
            if (name == to!string(e)) {
                return e;
            }
        }
    } else {
        immutable iname = toLower(name);
        foreach (e; EnumMembers!UT) {
            if (iname == toLower(to!string(e))) {
                return e;
            }
        }
    }
    return T.init;
}
unittest
{
    enum X { Planet, Moon }

    assert(getId!X("Moon") == X.Moon);
    assert(!getId!X("moon") == X.Moon);
    assert(!getId!X("MOON") == X.Moon);
    assert(getId!X("moon") == X.Planet);
    assert(getId!X("MOON") == X.Planet);

    assert(getId!X("Moon", Sensitive.No) == X.Moon);
    assert(getId!X("moon", Sensitive.No) == X.Moon);
    assert(getId!X("MOON", Sensitive.No) == X.Moon);
}



/**
 * Get the enumeration member identifier whose value is equivalent to the string
 * argument. Assumes that the string argument is a valid enumeration member
 * identifier. To check, use the `isId` function.
 *
 * Returns: An enumeration member identifier.
 */
T
getIdFromValue (T)(
    const string arg,
    Sensitive caseSensitive = Sensitive.Yes
)
if (is(T == enum))
{
    import std.traits: Unqual, EnumMembers;
    import std.uni: toLower;
    import std.conv: to;

    alias E = Unqual!T;

    if (caseSensitive == Sensitive.Yes) {
        foreach (e; EnumMembers!E) {
            if (arg == value(e)) {
                return e;
            }
        }
        return T.init;
    } else {
        immutable ivalue = toLower(arg);
        foreach (e; EnumMembers!E) {
            if (ivalue == toLower(value(e))) {
                return e;
            }
        }
        return T.init;
    }
}
unittest
{
    enum B : bool { No = false, Yes = true }
    assert(getIdFromValue!B("false") == B.No);
    assert(getIdFromValue!B("true") == B.Yes);

    enum I : int { First = 100, Second = 200, Third = 300 }
    assert(getIdFromValue!I("100") == I.First);
    assert(getIdFromValue!I("200") == I.Second);
    assert(getIdFromValue!I("300") == I.Third);

    enum S : string { Planet = "Mars", Moon = "Phobos" }
    assert(getIdFromValue!S("Mars") == S.Planet);
    assert(getIdFromValue!S("Phobos") == S.Moon);

    assert(getIdFromValue!S("mars", Sensitive.No) == S.Planet);
    assert(getIdFromValue!S("phobos", Sensitive.No) == S.Moon);
}
