module exp.enumeration.conversion;

public
import exp.enumeration.conversion.to_string;
import exp.enumeration.conversion.to_string_array;



debug
import std.stdio;



/**
 * Join the array argument using the specified format string.
 *
 * If format string is not specified, it defaults to '%s, ' where the last two
 * characters are assumed to be a separator which is excluded after the last
 * item.
 *
 * The function also accepts a second array of string which is formatted
 * according to the format string argument. The default format string when
 * the secondary array of string is present is `%s (%s), `.
 *
 * Returns: An array of elements.
 */
string
join (
    const string[] arg,
    const string fs = "%s, ",
    const string[] desc = []
) {
    import std.format: format;

    // TODO: Verify that the format string has '%s'.
    string fmts = fs;
    string line;
    if (desc.length == 0) {
        foreach (item; arg[0..$ - 1]) {
            line ~= format(fmts, item);
        }
        line ~= arg[$ - 1];
        return line;
    } else {
        immutable DEFAULT_FMTS = "%s, ";
        immutable WITH_DESC_FMTS = "%s (%s), ";
        fmts = fs == DEFAULT_FMTS ? WITH_DESC_FMTS : fs;
        foreach (i, item; arg[0..$ - 1]) {
            line ~= format(fmts, item, desc[i]);
        }
        line ~= format(fmts, arg[$ - 1], desc[$ - 1]);
        return line[0..$ - 2];
    }
}
unittest
{
    auto res = join(["None", "Alpha", "Digit"]);
    debug (verbose) writeln("Result: ", res);
    assert(res == "None, Alpha, Digit");
}
unittest
{
    auto res = join(["None", "Alpha", "Digit"], "%s; ");
    debug (verbose) writeln("Result: ", res);
    assert(res == "None; Alpha; Digit");
}
unittest
{
    auto res = join(["None", "Alpha", "Digit"], "%s [%s]; ", ["X", "A", "0"]);
    debug (verbose) writeln("Result: ", res);
    assert(res == "None [X]; Alpha [A]; Digit [0]");
}
