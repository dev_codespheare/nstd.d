module exp.enumeration.conversion.to_string;

import std.traits: Unqual, OriginalType, isSomeString;
debug import std.stdio;



/**
 * Get the enum `T` member identifier as a string.
 *
 * Returns: string
 */
string
member (T)(T arg)
if (is(Unqual!T == enum))
{
    import std.conv: to;
    return to!string(arg);
}
unittest
{
    enum B : bool { No = false, Yes = true }
    assert (member(B.No) == "No");
    assert (member(B.Yes) == "Yes");

    enum I : int { First = 100, Second = 200, Third = 300 }
    assert (member(I.First) == "First");
    assert (member(I.Second) == "Second");
    assert (member(I.Third) == "Third");

    enum S : string { Planet = "Mars", Moon = "Phobos" }
    assert (member(S.Planet) == "Planet");
    assert (member(S.Moon) == "Moon");

    enum F : float { Pi = 3.14159, Pythagoras = 1.41421 }
    assert (member(F.Pi) == "Pi");
    assert (member(F.Pythagoras) == "Pythagoras");
}



/**
 * Get the value of the enum `T` member argument as a string.
 *
 * Returns: string
 */
string
value (T)(T arg)
if (is(Unqual!T == enum) && isSomeString!(OriginalType!(Unqual!T)))
{
    import std.traits: Unqual, OriginalType;
    import std.conv: to;
    static assert (is(Unqual!T == enum), ERR_ENUM);
    alias UT = Unqual!T;
    alias BaseType = OriginalType!UT;
    debug (verbose) writeln("value(T == string)() called.");
    return cast(BaseType) arg;
}
unittest
{
    debug (verbose) writeln("BaseType string");
    enum S : string { Planet = "Mars", Moon = "Phobos" }
    assert (value(S.Planet) == "Mars");
    assert (value(S.Moon) == "Phobos");
}



/**
 * Get the value of the enum `T` member argument as a string.
 *
 * Returns: string
 */
string
value (T)(T arg)
if (is(Unqual!T == enum) && !isSomeString!(OriginalType!(Unqual!T)))
{
    import std.conv: to;
    alias UT = Unqual!T;
    alias BaseType = OriginalType!UT;
    debug (verbose) writeln("value(T != string)() called.");
    return to!string(cast(BaseType) arg);
}
unittest
{
    debug (verbose) writeln("BaseType not string:");
    enum B : bool { No = false, Yes = true }
    assert (value(B.No) == "false");
    assert (value(B.Yes) == "true");

    enum I : int { First = 100, Second = 200, Third = 300 }
    assert (value(I.First) == "100");
    assert (value(I.Second) == "200");
    assert (value(I.Third) == "300");

    enum F : float { Pi = 3.14159, Pythagoras = 1.41421 }
    assert (value(F.Pi) == "3.14159");
    assert (value(F.Pythagoras) == "1.41421");
}
