module exp.enumeration.conversion.to_string_array;

import std.traits: Unqual, OriginalType, isSomeString;
debug import std.stdio;



/**
 * Get all member identifiers of the enum template argument `T`.
 *
 * Returns: string[]
 */
string[]
members (T)()
if (is(Unqual!T == enum))
{
    alias UT = Unqual!T;
    return [__traits(allMembers, UT)];
}
unittest
{
    enum E: string {
        None = "",
        Alpha = "a..z",
        Digit = "0..9",
    }
    assert(members!E == ["None", "Alpha", "Digit"]);
}



/**
 * Get all values of the enum template argument `T` whose base type is `string`.
 * If the enumeration base type is not of type `string`, then the values are
 * converted to `string`.
 *
 * Returns: string[]
 */
string[]
values (T)()
if (is(Unqual!T == enum) && isSomeString!(OriginalType!(Unqual!T)))
{
    import std.traits: EnumMembers;
    alias UT = Unqual!T;
    alias BaseType = OriginalType!UT;
    string[] res;

    foreach (e; EnumMembers!UT) {
        res ~= cast(BaseType) e;
    }
    return res;
}
unittest
{
    enum E: string {
        None = "",
        Alpha = "a..z",
        Digit = "0..9",
    }
    assert(values!E == ["", "a..z", "0..9"]);
}



/**
 * Get all values of the enum template argument `T` whose base type is not
 * `string`.
 *
 * Returns: string[]
 */
string[]
values (T)()
if (is(Unqual!T == enum) && !isSomeString!(OriginalType!(Unqual!T)))
{
    import std.traits: EnumMembers;
    import std.conv: to;
    alias UT = Unqual!T;
    alias BaseType = OriginalType!UT;
    string[] res;

    foreach (e; EnumMembers!UT) {
        res ~= to!string(cast(BaseType) e);
    }
    return res;
}
unittest
{
    enum E {
        One = 100,
        Two = 200,
        Last = 999,
    }
    assert(values!E == ["100", "200", "999"]);
}
